<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Odojer extends Model
{
    protected $fillable = [
    		'odojer_name',
    		'odojer_address',
    		'odojer_location',
    		'odojer_gender',
    		'odojer_phone',
    		'odojer_status',
    		'program_id',
    		'city_id',
  	];

    public function programs()
    {
        return $this->belongsTo('App\Program', 'program_id');
    }

    public function cities()
    {
        return $this->belongsTo('App\City', 'city_id');
    }


}
