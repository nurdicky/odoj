<?php

namespace App\Http\Controllers;

use Alert;
use App\Odojer;
use App\Program;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class RegisterController extends Controller
{
    public function __construct()
    {
    	//
    }

    public function create()
    {

      return view('register', [
        'programs' => Program::all(),
    		'label' => 'Pendaftaran',
    		'button' => 'Daftar',
    		'app' => [
    			'name' => 'One Day One Juz',
    		],
    	]);

    }

    public function store(Request $request)
    {
    	$odojers = Odojer::create($request->all());
    	return redirect()->route('notif', ['odojers' => $odojers]);
    }
}
