<?php

namespace App\Http\Controllers;

use App\Group;
use App\Odojer;
use App\Program;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class NotificationController extends Controller
{
    public function __construct()
    {
    	//
    }

    public function index(Request $id)
    {
        $odojers = Odojer::find($id)->first();
        $odojerProgram = $odojers->program_id;
        $odojerGender = substr($odojers->odojer_gender, 0, 1);

        $group = Group::where('group_type', 'LIKE', $odojerGender)
              ->Where('group_program', $odojerProgram)
              ->where('group_active', 1)
              ->first();

        $program = Program::where('id', $group->group_program)->first();
        $totalNow = $group->group_total_now + 1;
        $updateGroup = Group::where('id', $group->id)
              ->update(['group_total_now' => $totalNow]);

        return view('notification', ['label'=> 'Groups', 'groups' => $group, 'programs' => $program]);
    }

    public function create()
    {
    	//
    }

    public function show($id)
    {
        // $groups = Group::all();
        // return view('admin.groups.detail', ['label'=> 'Groups', 'groups' => $groups]);
    }

    public function store(Request $request)
    {
    	// $groups = Group::create($request->all());
    	// return redirect()->route('notif');
    }
}
