<?php

namespace App\Http\Controllers\Admin;

use DB;
use Chart;
use App\Odojer;
use App\City;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class DashboardController extends Controller
{
    public function index()
    {
      $odojersCount = Odojer::count();
      $odojersDaySign = Odojer::where(DB::raw('DATE(created_at)'), '=', Carbon::now()->format('Y-m-d'))->count();
      // dd($odojersCount);
      return view('admin/index', [
        'count_odoj' => Odojer::where('program_id', 1)->count(),
        'count_odalf' => Odojer::where('program_id', 2)->count(),
        'count_odol_kids' => Odojer::where('program_id', 3)->count(),
        'count_odoj_star' => Odojer::where('program_id', 4)->count(),
        'count_odojer' => $odojersCount,
        'odojer_day_sign' => $odojersDaySign,
      ]);
    }

    public function chart()
    {
      $odojers = Odojer::with('cities')->get();
      $countCity = City::count();
      $cities = City::all();

        // switch ($odojer->city_id) {
        //   case 1:
        //     $counts = Odojer::where([
        //       [DB::raw('MONTH(created_at)'), '=', Carbon::now()->format('m')],
        //       ['city_id', $odojer->city_id],
        //     ])->count();
        //     dump($odojer->cities->city_name);
        //     dump($counts);
        //     break;
        //   case 2:
        //     $counts = Odojer::where([
        //       [DB::raw('MONTH(created_at)'), '=', Carbon::now()->format('m')],
        //       ['city_id', $odojer->city_id],
        //     ])->count();
        //     dump($odojer->cities->city_name);
        //     dump($counts);
        //     break;
        //   case 3:
        //     break;
        //
        //   default:
        //     break;
        // }

      $charts = [
          'chart' => ['type' => 'line'],
          'title' => ['text' => 'Grafik Pendaftaran Odojer Wilayah Jawa Timur'],
          'xAxis' => [
              'categories' => [
                'Januari' , 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'Sepetember', 'Oktober', 'November', 'Desember'
              ],
          ],
          'yAxis' => [
              'title' => [
                  'text' => 'Jumlah Odojer'
              ]
          ],
          'series' => [
                [
                    'name' => 'Surabaya',
                    'data' => [0,30,200],

                ],

          ]
      ];

      $arr            = array();
      $chart_temp     = array();
      $temp           = array();
      $chart          = array();
      $type_chart     = array();
      foreach ($odojers as $odojer) {
        $cityTemp = array();
        for ($i=0; $i < $countCity ; $i++) {
            if ($odojer->city_id == $cities[$i]->id) {
              for ($j=0; $j < 12; $j++) {
                $data = array_set($cityTemp, 'city_name',  $odojer->cities->city_name);
                // dump($cityTemp);
                $month = sprintf("%02d", $j);
                $counts = Odojer::where([
                  [DB::raw('MONTH(created_at)'), '=', $month],
                  ['city_id', $odojer->city_id],
                ])->count();
                $data = array_set($temp, $j, $counts);
              }
              // if ($cityTemp[]) {
              //   // code...
              // }
              $data = array_set($arr, 'name',  $odojer->cities->city_name);
              // dump($arr);
              // $data = array_set($arr, 'data',  $temp);
              // $data = array_set($chart_temp, $i,  $data);
              // $data = array_set($chart, 'series',  $chart_temp);

              // dump($arr);

              // $data = array_set($chart, 'type',  'line');
              break;
            }
            // dump($odojer->cities->city_name, $i);
            // dump($odojer->cities->city_name);
            // break;
        }
      }

      return Chart::display("id-highchartsnya", $charts);
    }
}
