<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Program;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::with('programs')->get();
        return view('admin.groups.list', ['label'=> 'Groups', 'groups' => $groups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $programs = Program::all();
        return view('admin.groups.form', ['label'=> 'Groups', 'programs' => $programs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $groups = Group::create([
          'group_name' => $request->group_name,
          'group_link' => $request->group_link,
          'group_type' => $request->group_type,
          'group_program' => $request->group_program,
          'group_capacity' => $request->group_capacity,
          'group_total_now' => 0,
          'group_active' => $request->group_active,
        ]);

        return redirect()->route('group.index')->with('alert-success','Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $groups = Group::find($id);
        return view('admin.groups.detail', ['label'=> 'Groups', 'groups' => $groups]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groups = Group::find($id);
        $programs = Program::all();
        return view('admin.groups.form', ['label'=> 'Groups', 'groups' => $groups, 'programs' => $programs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $groups = Group::find($id);
        $groups->group_name = $request->group_name;
        $groups->group_link = $request->group_link;
        $groups->group_type = $request->group_type;
        $groups->group_program = $request->group_program;
        $groups->group_capacity = $request->group_capacity;
        $groups->group_active = $request->group_active;
        $groups->save();

        return redirect()->route('group.index')->with('alert-success','Berhasil Memperbarui Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::destroy($id);
        return redirect()->route('group.index')->with('alert-success','Berhasil Menghapus Data!');
    }
}
