<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Odojer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class OdojersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $odojers = Odojer::with('programs', 'cities')->orderBy('id', 'DESC')->get();
        return view('admin.odojers.list', ['label'=> 'Odojers', 'odojers' => $odojers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.odojers.form', ['label'=> 'Odojers']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $odojers = Odojer::create($request->all());
        return redirect()->route('odojer.index')->with('alert-success','Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $odojers = Odojer::find($id);
        return view('admin.odojers.detail', ['label'=> 'Odojers', 'odojers' => $odojers]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $odojers = Odojer::find($id);
        return view('admin.odojers.form', ['label'=> 'Odojers', 'odojers' => $odojers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $odojers = Odojer::find($id);
        $odojers->odojer_name       = $request->odojer_name;
        $odojers->odojer_address    = $request->odojer_address;
        $odojers->odojer_location   = $request->odojer_location;
        $odojers->odojer_gender     = $request->odojer_gender;
        $odojers->odojer_phone      = $request->odojer_phone;
        $odojers->odojer_status     = $request->odojer_status;
        $odojers->program_id        = $request->program_id;
        $odojers->city_id           = $request->city_id;
        $odojers->save();

        return redirect()->route('odojer.index')->with('alert-success','Berhasil Memperbarui Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $odojer = Odojer::destroy($id);
        return redirect()->route('odojer.index')->with('alert-success','Berhasil Menghapus Data!');
    }

    public function getByColumn($request)
    {
      $odojers = DB::table('odojers')
                ->select($request)
                ->groupBy($request)
                ->get();
      return Response::JSON($odojers);
    }

    public function filterAll(Request $request)
    {
      $filter     = $request->filter;
      $subFilter  = $request->sub_filter;
      $odojers    = DB::table('odojers')
                    ->select('*')
                    ->where($filter, $subFilter)
                    ->get();

      return Response::json($odojers);
    }
}
