<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $fillable = [
    		'program_name',
    		'program_slug',
    		'program_desc',
  	];

    public function groups() {
  		  return $this->hasMany('App\Group', 'group_program', 'id');
  	}

    public function odojers() {
  		  return $this->hasMany('App\Odojer', 'program_id', 'id');
  	}

}
