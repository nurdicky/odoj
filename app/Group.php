<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
	protected $fillable = [
		'group_name',
		'group_link',
		'group_type',
		'group_program',
		'group_capacity',
		'group_total_now',
		'group_active',

	];

	public function programs()
  {
      return $this->belongsTo('App\Program', 'group_program');
  }

}
