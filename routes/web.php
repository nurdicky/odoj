<?php
// Route::get('/', function () {
//     // return view('welcome');
//     return view('chart');
// });

//Public Routing
Route::get('/chart', 'Admin\DashboardController@chart');
Route::get('/', 'RegisterController@create')->name('signup.create');
Route::post('/signup', 'RegisterController@store')->name('signup.store');
Route::get('/notif', 'NotificationController@index')->name('notif');
Route::get('/odojer_name', function($odojer){
	return View::make('chats')->with('odojer_name', $odojer);
});

Auth::routes();

//Private Routing
Route::group(['middleware' => 'admin'],function(){
  Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');
  Route::resource('group', 'Admin\GroupsController');
	Route::resource('program', 'Admin\ProgramsController');
	Route::resource('city', 'Admin\CitiesController');

  //JSON request
  Route::post('/odojer/filter', 'Admin\OdojersController@filterAll')->name('odojer.filter');
  Route::get('/odojer/column/{column}', 'Admin\OdojersController@getByColumn')->name('odojer.gender');
	Route::resource('odojer', 'Admin\OdojersController');

});
