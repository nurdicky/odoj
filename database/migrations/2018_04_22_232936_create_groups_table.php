<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
          $table->increments('id');
          $table->string('group_name', 75);
          $table->string('group_link', 100);
          $table->string('group_type');
          $table->unsignedInteger('group_program');
          $table->integer('group_capacity');
          $table->integer('group_total_now');
          $table->integer('group_active');
          $table->timestamps();

          $table->foreign('group_program')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
