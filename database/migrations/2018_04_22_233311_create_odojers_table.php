<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOdojersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odojers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('odojer_name', 100);
            $table->text('odojer_address');
            $table->string('odojer_location', 100);
            $table->string('odojer_gender', 50);
            $table->string('odojer_phone', 15);
            $table->integer('odojer_status');
            $table->unsignedInteger('program_id');
            $table->unsignedInteger('city_id');
            $table->timestamps();

            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odojers');
    }
}
