@extends('layouts.guest.master')

@section('content')


<div class="bg-light feature up">
    <div class="container">
        <div class="spacer">

          <div class="container">
              <div class="row justify-content-center">
                  <div class="col-md-7 text-center p-t-10 p-b-10">
                      <h1 class="title font-bold">SELAMAT</h1>
                      <h6 class="subtitle">
                        Selamat Anda telah terdaftar di program {{$programs->program_slug}} ({{$programs->program_name}}).<br>
                        Silahkan klik link dibawah untuk masuk ke grup Anda.
                      </h6>

                  </div>
                  <div class="col-md-12 m-t-20 p-t-20 p-b-20 text-center">
                    <a href="{{$groups->group_link}}" class="btn btn-outline-success btn-lg">
                      {{$groups->group_link}}
                    </a>
                  </div>
              </div>
          </div>

        </div>
    </div>
</div>

@endsection
