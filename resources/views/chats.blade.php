<!DOCTYPE html>
<html>
<head>
    <title>CHATS</title>
    <link href="{{asset('../css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('../css/chats.css')}}" rel="stylesheet">
</head>
<body>

    <div class="col-lg-4 col-lg-offset-4">
        <h1 id="greeting">Hello, <span id="odojer_name">{{$odojer_name}}</span></h1>

        <div id="chat-window" class="col-lg-12">
        </div>
        <div class="col-lg-12">
            <input type="text" id="text" class="form-control col-lg-12" autofocus="">
        </div>
    </div>

    <script src="{{asset('../js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{asset('../js/chats.js')}}"></script>

</body>
</html>