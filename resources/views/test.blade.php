<script src="{{ asset('bower_components/jquery/dist/jquery.min.js')}}"></script>

<div id="filter" class="input-group input-group-sm pull-left" style="margin-right:10px">
  <select class="form-control select2" style="width: 100%;">
    <option value="odojer_gender">Jenis Kelamin</option>
    <option value="city_id">Kode Kota</option>
    <option value="program_id">Program</option>
    <option value="status">Status</option>
  </select>
</div>

<script type="text/javascript">

  $("#filter select").change(function () {
    var str = $("select option:selected").val()
    console.log(str);
  });

</script>
