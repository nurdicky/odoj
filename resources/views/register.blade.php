@extends('layouts.guest.master')

@section('content')

<div class="bg-light feature up">
    <div class="container">
        <div class="spacer" id="content">
            <div class="row m-0">
                <div class="col-lg-12">
                  <form data-aos="fade-left" data-aos-duration="1200"class="form-horizontal" method="POST" action="{{ route('signup.store') }}">
                      {{ csrf_field() }}

                      <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group m-t-1">
                              <span id=kode class="label label-danger label-rounded">#S00B45Y</span>
                              <input id="city_id" type="hidden" class="form-control" name="city_id" value="1">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-t-15 {{ $errors->has('odojer_name') ? ' has-error' : '' }}">
                                <input id="odojer_name" type="text" placeholder="Nama Lengkap" class="form-control" name="odojer_name" value="{{ old('odojer_name') }}" required autofocus>
                                @if ($errors->has('odojer_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('odojer_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6 {{ $errors->has('odojer_phone') ? ' has-error' : '' }}">
                            <div class="form-group m-t-15">
                              <input id="odojer_phone" placeholder="Nomor Whatsapp" type="text" class="form-control" name="odojer_phone" value="{{ old('odojer_phone') }}" required>
                                @if ($errors->has('odojer_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('odojer_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group m-t-6 {{ $errors->has('odojer_location') ? ' has-error' : '' }}">
                                <input id="odojer_location" placeholder="Kota / Kabupaten" type="text" class="form-control" name="odojer_location" value="{{ old('odojer_location') }}" required>
                                @if ($errors->has('odojer_location'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('odojer_location') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-t-6 {{ $errors->has('odojer_gender') ? ' has-error' : '' }}">
                                <select class="form-control" name="odojer_gender">
                                  <option value="Laki-Laki">Laki-Laki</option>
                                  <option value="Perempuan">Perempuan</option>
                                </select>
                                <!-- <input id="odojer_gender" placeholder="Jenis Kelamin" type="text" class="form-control" name="odojer_gender" value="{{ old('odojer_gender') }}" required>
                                @if ($errors->has('odojer_gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('odojer_gender') }}</strong>
                                    </span>
                                @endif -->
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-t-6 {{ $errors->has('odojer_address') ? ' has-error' : '' }}">
                                <input id="odojer_address" placeholder="Alamat" type="text" class="form-control" name="odojer_address" value="{{ old('odojer_address') }}" required>
                                @if ($errors->has('odojer_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('odojer_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-t-6 {{ $errors->has('program_id') ? ' has-error' : '' }}">
                                <select class="form-control" name="program_id">
                                  @foreach($programs as $program)
                                    <option value="{{$program->id}}">{{$program->program_slug}} ( {{$program->program_name}})</option>
                                  @endforeach
                                </select>
                                <!-- <input id="program_id" placeholder="Jenis Program" type="text" class="form-control" name="program_id" value="{{ old('program_id') }}" required>
                                @if ($errors->has('program_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('program_id') }}</strong>
                                    </span>
                                @endif -->
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group m-t-15">
                              <input id="odojer_status" type="hidden" class="form-control" name="odojer_status" value="0">
                            </div>
                        </div>
                        <!-- <div class="col-lg-6">
                            <div class="form-group m-t-15">
                                <textarea class="form-control" rows="3" placeholder="Alamat"></textarea>
                            </div>
                        </div> -->
                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-md m-t-30 btn-success-gradiant font-14">DAFTAR</button>
                        </div>
                      </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
