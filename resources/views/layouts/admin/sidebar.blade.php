<section class="sidebar">
  <!-- Sidebar user panel -->
  <!-- <div class="user-panel">
    <div class="pull-left image">
      <img src="dist/img/laravel-indonesia.png" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
      <p>Sector Code</p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div> -->
  <!-- search form -->
  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
    </div>
  </form>
  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>


    <li class="<?php if (Route::getCurrentRoute()->uri == 'dashboard') {
      echo "active";
    } ?>">
      <a href="{{ route('dashboard') }}">
        <i class="fa fa-home"></i> <span>Dashboard</span>
        <!-- <span class="pull-right-container">
          <small class="label pull-right bg-green">new</small>
        </span> -->
      </a>
    </li>
    <!-- <li class="treeview">
    </li> -->
    <li class="<?php if (Route::getCurrentRoute()->uri == 'group') {
      echo "active";
    } ?>">
      <a href="{{ route('group.index') }}">
        <i class="fa fa-user-circle"></i> <span>List Group</span>
      </a>
    </li>
    <li class="<?php if (Route::getCurrentRoute()->uri == 'program') {
      echo "active";
    } ?>">
      <a href="{{ route('program.index') }}">
        <i class="fa fa-table"></i> <span>List Program</span>
      </a>
    </li>
    <li class="<?php if (Route::getCurrentRoute()->uri == 'city') {
      echo "active";
    } ?>">
      <a href="{{ route('city.index') }}">
        <i class="fa fa-table"></i> <span>List Kota</span>
      </a>
    </li>
    <li class="<?php if (Route::getCurrentRoute()->uri == 'odojer') {
      echo "active";
    } ?>">
      <a href="{{ route('odojer.index') }}">
        <i class="fa fa-users"></i> <span>List Pendaftar</span>
      </a>
    </li>
    
  </ul>
</section>
