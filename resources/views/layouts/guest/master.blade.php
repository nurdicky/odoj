<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('guest/assets/images/favicon.png') }}"> -->
    <title>{{ config('app.name', 'ODOJ') }}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('guest/assets/node_modules/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- This page CSS -->
    <!-- Custom CSS -->
    <link href="{{ asset('guest/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('guest/css/index-landingpage/landing-page.css') }}" rel="stylesheet">
    <link href="{{ asset('guest/css/guest.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="">

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
      <!-- ============================================================== -->
      <!-- Top header  -->
      <!-- ============================================================== -->
      <!-- <div class="topbar" id="top">
          <div class="header6">
              <div class="container po-relative">
                  <nav class="navbar navbar-expand-lg h6-nav-bar">
                      <a href="index.html" class="navbar-brand"><img src="{{ asset('guest/assets/images/logos/white-text.png') }}" alt="wrapkit" /></a>
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#h6-info" aria-controls="h6-info" aria-expanded="false" aria-label="Toggle navigation"><span class="ti-menu"></span></button>
                      <div class="collapse navbar-collapse hover-dropdown font-14 ml-auto" id="h6-info">
                          <ul class="navbar-nav ml-auto">
                              <li class="nav-item">
                                  <a class="nav-link" href="#section">
                                      Sections
                                  </a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="elements.html#element">
                                      Elements
                                  </a>
                              </li>
                          </ul>
                          <div class="act-buttons">
                              <a href="https://wrappixel.com/templates/wrapkit/" class="btn btn-success-gradiant font-14">Upgrade To Pro</a>
                          </div>
                      </div>
                  </nav>
              </div>
          </div>
      </div> -->
      <!-- ============================================================== -->
      <!-- Top header  -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Page wrapper  -->
      <!-- ============================================================== -->
      <div class="page-wrapper">
          <!-- ============================================================== -->
          <!-- Container fluid  -->
          <!-- ============================================================== -->

          <div class="container-fluid">

            <div class="static-slider10">
                <div class="container">
                    <!-- Row  -->
                    <div class="row justify-content-center ">
                        <!-- Column -->
                        <div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
                            <span class="label label-rounded label-success">Pendaftaran Calon Anggota</span>
                            <h1 class="title">One Day One Juz</h1>
                        </div>
                        <!-- Column -->

                    </div>
                </div>
            </div>

            @yield('content')

          </div>

          <!-- ============================================================== -->
          <!-- End Container fluid  -->
          <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- End Page wrapper  -->
      <!-- ============================================================== -->

      <div class="footer4 bg-dark ">
          <div class="container">
              <div class="f4-bottom-bar">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="d-flex font-14">
                              <div class="m-t-15 m-b-20 copyright" id="copyright"> &copy One Day One Juz - {{ Date('Y') }}</div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('guest/assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="{{ asset('guest/assets/node_modules/popper/dist/popper.min.js') }}"></script>
    <script src="{{ asset('guest/assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('guest/js/custom.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    @include('sweet::alert')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script type="text/javascript">

        $('a').on('click', function (event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - 90
            }, 1000);
            event.preventDefault();
            // code

        });
    </script>
</body>

</html>
