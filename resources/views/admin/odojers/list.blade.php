@extends('layouts.admin.master')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      List Pendaftaran
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data {{ $label }}</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
              <a class="btn btn-success" href="{{ route('odojer.create') }}">Create New Odojer</a>
            </h3>
            <div class="box-tools">
              <form id="form-filter" class="" action="" method="post">
                <div id="filter" class="input-group input-group-sm pull-left" style="margin-right:10px">
                  <select id="first" name="filter" class="form-control select2" style="width: 100%;">
                    <option value="odojer_gender" selected="selected">Jenis Kelamin</option>
                    <option value="city_id">Kode Kota</option>
                    <option value="program_id">Program</option>
                    <option value="odojer_status">Status</option>
                  </select>
                </div>
                <div id='sub-filter' class="input-group input-group-sm pull-left" style="margin-right:5px">
                  <select id="second" name="sub_filter" class="form-control select2" style="width: 100%;">
                    <option value="Laki-Laki" selected="selected">Laki-Laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
                <div id="submit-filter" class="input-group input-group-sm pull-left" style="margin-right:20px">
                    <button type="submit" class="btn btn-sm btn-default">filter</button>
                </div>
                <div class="input-group input-group-sm" style="width: 150px; ">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">

            @if(Session::has('alert-success'))
              <div class="alert alert-success" style="background-color:#dff0d8 !important; color:#00a65a !important">
                  <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
              </div>
            @endif

            <table class="table table-stripped table-hover">
              <thead>
                <th>#</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Kota/Kabupaten</th>
                <th>Jenis Kelamin</th>
                <th>No. Whatsapp</th>
                <th>Status Anggota</th>
                <th>Program Pilihan</th>
                <th>Link Kota</th>
                <th>Opsi</th>
                <th></th>
              </thead>
              <tbody id="#list-odojer">
                @php $no = 1; @endphp
                @foreach ($odojers as $odojer)
                  <tr id="odojer{{$odojer->id}}">
                    <td>{{ $no++ }}</td>
                    <td>{{ $odojer->odojer_name }}</td>
                    <td>{{ $odojer->odojer_address }}</td>
                    <td>{{ $odojer->odojer_location }}</td>
                    <td>{{ $odojer->odojer_gender }}</td>
                    <td>{{ $odojer->odojer_phone }}</td>
                    <td class="text-center">
                      @if($odojer->odojer_status == 1)
                        <label class="label label-success">Active</label>
                      @else
                        <label class="label label-danger">Non active</label>
                      @endif
                    </td>
                    <td>{{ $odojer->programs->program_slug }}</td>
                    <td>{{ $odojer->cities->city_code }}</td>
                    <td>
                      <form action="{{ route('odojer.destroy', $odojer->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <a class="btn btn-sm btn-warning" href="{{ route('odojer.edit', $odojer->id) }}">Edit</a>
                        <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                      </form>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
  </section>

</div>

<meta name="_token" content="{!! csrf_token() !!}" />


@endsection

@section('custom')
<script type="text/javascript">
  var host = window.location.hostname;
  var path = window.location.pathname;

  $("#filter #first").change(function () {
    var str = $("#first option:selected").val();
    var url = 'http://'+host+path+'/column/'+str;

    //request data
    var data = JSON.parse($.ajax({
      type: 'GET',
      url : url,
      dataType:'json',
      global: false,
      async: false,
      success:function(msg){
      }
    }).responseText);
    var htmlStr = "";

    for (var i = 0; i < data.length; i++) {
      var arr = data[i];
      var key = Object.keys(arr);
      var val = Object.values(arr);
      if (key == str) {
        htmlStr = htmlStr + '<option value="'+val+'"> '+ val +' </option>';
      }
      //Memperbarui select sub filter
      $('#sub-filter #second option').remove();
      $('#sub-filter #second').prepend(htmlStr);
    }

  });

  $("#sub-filter #second").change(function () {
    var str = $("#second option:selected").val();
    var url = 'http://'+host+path+'/column/'+str;
    console.log(str);
  });

  $('#submit-filter').click(function(e){
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    e.preventDefault();
    var datatosend = $('#form-filter').serialize();
    console.log("datatosend : "+ datatosend);

    $.ajax({
      dataType : 'json',
      type : 'POST',
      url : 'http://'+host+path+'/filter',
      data : datatosend,
      success : function(data) {
        console.log(data);
        $("table tbody tr").remove();

        $.each(data, function(index, element) {
          console.log(element);

          var tabel = '';
          tabel = '<tr id="product' + element.id + '"><td><input type="checkbox" name="id[]" value="22"></td><td>' + element.odojer_name + '</td><td>' + element.odojer_address + '</td><td>' + element.odojer_location + '</td><td>' + element.odojer_gender + '</td><td>' + element.odojer_phone + '</td><td>' + element.odojer_status  + '</td><td>' + element.program_id  + '</td>';
          tabel += '</td><td>' + element.city_id  + '</td>';

          tabel += '<td>'
          tabel += '<form action="http://'+host+path+'/'+element.id+'" method="post">';
          tabel += '{{ csrf_field() }}';
          tabel += '{{ method_field('DELETE') }}';
          tabel += '<a class="btn btn-sm btn-warning" href="http://'+host+path+'/'+element.id+'/edit">Edit</a>';
          tabel += ' <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm(Yakin ingin menghapus data?)">Delete</button>';
          tabel += '</form>';
          tabel += '</td>';
          // tabel += '<td><button class="btn hidden-sm-down btn-info open-modal" value="' + element.id + '">Edit</button> ';
          // tabel += ' <button class="btn hidden-sm-down btn-danger delete-item" value="' + element.id + '">Delete</button></td></tr>';

          // if (state == "add"){ //if user added a new record
          $('table').find('tbody').append(tabel);

        });

      }
    });

  });


</script>
@endsection
