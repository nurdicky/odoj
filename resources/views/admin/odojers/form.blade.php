@extends('layouts.admin.master')

@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @if (@$odojers->exists)
          Perbarui Data Anggota
      @else
          Tambah Anggota Baru
      @endif
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data {{ $label }}</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
            </h3>
            <div class="box-tools">
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            @if(@$odojers->exists) <?php  $url = route('odojer.update', @$odojers->id); ?>
            @else <?php  $url = route('odojer.store'); ?>
            @endif
          <form class="form-horizontal" action="{{ $url }}" method="POST">

                @if (@$odojers->exists)
                    {{ method_field('PUT') }}
                @else
                    {{ method_field('POST') }}
                @endif

                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('odojer_name') ? ' has-error' : '' }}">
                    <label for="odojer_name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input id="odojer_name" type="text" class="form-control" name="odojer_name" value="{{ @$odojers->odojer_name }}" required>

                        @if ($errors->has('odojer_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('odojer_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('odojer_address') ? ' has-error' : '' }}">
                    <label for="odojer_address" class="col-md-4 control-label">Alamat</label>

                    <div class="col-md-6">
                        <input id="odojer_address" type="text" class="form-control" name="odojer_address" value="{{ @$odojers->odojer_address }}"required>

                        @if ($errors->has('odojer_address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('odojer_address') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('odojer_location') ? ' has-error' : '' }}">
                    <label for="odojer_location" class="col-md-4 control-label">Kota/Kabupaten</label>

                    <div class="col-md-6">
                        <input id="odojer_location" type="text" class="form-control" name="odojer_location" value="{{ @$odojers->odojer_location }}"required>

                        @if ($errors->has('odojer_location'))
                            <span class="help-block">
                                <strong>{{ $errors->first('odojer_location') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('odojer_gender') ? ' has-error' : '' }}">
                    <label for="odojer_gender" class="col-md-4 control-label">Jenis Kelamin</label>

                    <div class="col-md-6">
                        <input id="odojer_gender" type="text" class="form-control" name="odojer_gender" value="{{ @$odojers->odojer_gender }}"required>

                        @if ($errors->has('odojer_gender'))
                            <span class="help-block">
                                <strong>{{ $errors->first('odojer_gender') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('odojer_phone') ? ' has-error' : '' }}">
                    <label for="odojer_phone" class="col-md-4 control-label">No. Whatsapp</label>

                    <div class="col-md-6">
                        <input id="odojer_phone" type="text" class="form-control" name="odojer_phone" value="{{ @$odojers->odojer_phone }}"required>

                        @if ($errors->has('odojer_phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('odojer_phone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('odojer_status') ? ' has-error' : '' }}">
                    <label for="odojer_status" class="col-md-4 control-label">Status Anggota</label>

                    <div class="col-md-6">
                        <input id="odojer_status" type="text" class="form-control" name="odojer_status" value="{{ @$odojers->odojer_status }}"required>

                        @if ($errors->has('odojer_status'))
                            <span class="help-block">
                                <strong>{{ $errors->first('odojer_status') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('program_id') ? ' has-error' : '' }}">
                    <label for="program_id" class="col-md-4 control-label">Program Pilihan</label>

                    <div class="col-md-6">
                        <input id="program_id" type="text" class="form-control" name="program_id" value="{{ @$odojers->program_id }}"required>

                        @if ($errors->has('program_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('program_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
                    <label for="city_id" class="col-md-4 control-label">Link Kota</label>

                    <div class="col-md-6">
                        <input id="city_id" type="text" class="form-control" name="city_id" value="{{ @$odojers->city_id }}"required>

                        @if ($errors->has('city_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('odojer_status') ? ' has-error' : '' }}">
                    <label for="odojer_status" class="col-md-4 control-label">Status</label>

                    <div class="col-md-6">
                        <select class="form-control" name="odojer_status">
                          <?php if (@$odojers->odojer_status == 1): ?>
                            <option value="1" selected>Active</option>
                            <option value="0" >Non Active</option>
                          <?php else: ?>
                            <option value="1" >Active</option>
                            <option value="0" selected>Non Active</option>
                          <?php endif; ?>
                        </select>

                        @if ($errors->has('odojer_status'))
                            <span class="help-block">
                                <strong>{{ $errors->first('odojer_status') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-success">
                            @if (@$odojers->exists)
                                Update {{ $label }}
                            @else
                                Add {{ $label }}
                            @endif
                        </button>
                        <a class="btn btn-primary" href="{{ route('odojer.index') }}">Back</a>
                    </div>
                </div>
            </form>

          </div>
        </div>
      </div>
    </div>
  </section>

</div>

@endsection
