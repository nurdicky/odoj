@extends('layouts.admin.master')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @if (@$cities->exists)
          Perbarui City
      @else
          Tambah City
      @endif
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data {{ $label }}</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
            </h3>
            <div class="box-tools">

            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">

              @if(@$cities->exists) <?php  $url = route('city.update', @$cities->id); ?>
              @else <?php  $url = route('city.store'); ?>
              @endif
              <form class="form-horizontal" action="{{ $url }}" method="POST">

                @if (@$cities->exists)
                    {{ method_field('PUT') }}
                @else
                    {{ method_field('POST') }}
                @endif

                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('city_name') ? ' has-error' : '' }}">
                    <label for="city_name" class="col-md-4 control-label">Nama Kota</label>

                    <div class="col-md-6">
                        <input id="city_name" type="text" class="form-control" name="city_name" value="{{ @$cities->city_name }}" required>

                        @if ($errors->has('city_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('city_code') ? ' has-error' : '' }}">
                    <label for="city_code" class="col-md-4 control-label">Kode Kota</label>

                    <div class="col-md-6">
                        <input id="city_code" type="text" class="form-control" name="city_code" value="{{ @$cities->city_code }}"required>

                        @if ($errors->has('city_code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city_code') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('city_url') ? ' has-error' : '' }}">
                    <label for="city_url" class="col-md-4 control-label">URL</label>

                    <div class="col-md-6">
                        <input id="city_url" type="text" class="form-control" name="city_url" value="{{ @$cities->city_url }}"required>

                        @if ($errors->has('city_url'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city_url') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-success">
                            @if (@$cities->exists)
                                Update {{ $label }}
                            @else
                                Add {{ $label }}
                            @endif
                        </button>
                        <a class="btn btn-primary" href="{{ route('city.index') }}">Back</a>

                    </div>
                </div>
              </form>

          </div>
        </div>
      </div>
    </div>
  </section>

</div>


@endsection
