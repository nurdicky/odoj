@extends('layouts.admin.master')

@section('content')
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Pendaftar Komunitas One Day One Juz
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Pendaftar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="form-group">
                <label>Filter</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Umur <= 12 Tahun</option>
                  <option>Umur <= 12 Tahun</option>
                  <option>Umur => 12 Tahun</option>
                  <option>Jenis Kelamin</option>
                  <option>Pendaftar Tiap Kota</option>
                  <option>Program Dipilih</option>
                  <option>Jumlah Anggota</option>

                </select>
              </div>
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Kota/Kabupaten</th>
                  <th>Jenis Kelamin</th>
                  <th>No. Whatsapp</th>
                  <th>Program Dipilih</th>
                  <th>Status Anggota</th>
                  <th>Link Kota</th>
                  <th>Opsi</th>
                </tr>
                <tr>
                  <th>Hani</th>
                  <th>Surabaya</th>
                  <th>Kota Surabaya</th>
                  <th>Wanita</th>
                  <th>08978997204</th>
                  <th>ODOJ KIDS</th>
                  <th>Aktif</th>
                  <th>2</th>
                  <th><button type="button" class="btn btn-warning">Edit</button>
                    <button type="button" class="btn btn-danger">Hapus</button></th>
                  </th>
                </tr>
              </table>
          </div>
                  </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <!--
    <section class="content-header">
      <h1>
        Daftar Link Pendaftar
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Daftar</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
            <div class="box-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
          </div>

          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tr>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Kota/Kabupaten</th>
                  <th>Jenis Kelamin</th>
                  <th>No. Whatsapp</th>
                  <th>Program Dipilih</th>
                  <th>Status Anggota</th>
                </tr>
                <tr>
                  <th>Hani</th>
                  <th>Surabaya</th>
                  <th>Kota Surabaya</th>
                  <th>Wanita</th>
                  <th>08978997204</th>
                  <th>ODOJ KIDS</th>
                  <th>Aktif</th>
                  </th>
                </tr>
              </table>
          </div>
        </div>
      </div>
    </section>
  </div> -->
@endsection
