@extends('layouts.admin.master')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      List Program
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data {{ $label }}</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
              <a class="btn btn-success" href="{{ route('program.create') }}">Create New Program</a>
            </h3>
            <div class="box-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">

            @if(Session::has('alert-success'))
              <div class="alert alert-success" style="background-color:#dff0d8 !important; color:#00a65a !important">
                  <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
              </div>
            @endif


              <table class="table table-stripped table-hover">
                <thead>
                  <th>#</th>
                  <th>Name</th>
                  <th>Slug</th>
                  <th>Description</th>
                  <th></th>
                </thead>
                <tbody>
                  @php $no = 1; @endphp
                  @foreach ($programs as $program)
                    <tr>
                      <td>{{ $no++ }}</td>
                      <td>{{ $program->program_name }}</td>
                      <td>{{ $program->program_slug }}</td>
                      <td>{{ $program->program_desc }}</td>
                      <td>
                        <form action="{{ route('program.destroy', $program->id) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <a class="btn btn-sm btn-warning" href="{{ route('program.edit', $program->id) }}">Edit</a>
                          <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>


          </div>
        </div>
      </div>
    </div>
  </section>

</div>

@endsection
