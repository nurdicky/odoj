@extends('layouts.admin.master')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @if (@$odojers->exists)
          Perbarui Program
      @else
          Tambah Program
      @endif
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data {{ $label }}</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
            </h3>
            <div class="box-tools">

            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">

              @if(@$programs->exists) <?php  $url = route('program.update', @$programs->id); ?>
              @else <?php  $url = route('program.store'); ?>
              @endif
              <form class="form-horizontal" action="{{ $url }}" method="POST">

                @if (@$programs->exists)
                    {{ method_field('PUT') }}
                @else
                    {{ method_field('POST') }}
                @endif

                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('program_name') ? ' has-error' : '' }}">
                    <label for="program_name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input id="program_name" type="text" class="form-control" name="program_name" value="{{ @$programs->program_name }}" required>

                        @if ($errors->has('program_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('program_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('program_slug') ? ' has-error' : '' }}">
                    <label for="program_slug" class="col-md-4 control-label">Slug</label>

                    <div class="col-md-6">
                        <input id="program_slug" type="text" class="form-control" name="program_slug" value="{{ @$programs->program_slug }}"required>

                        @if ($errors->has('program_slug'))
                            <span class="help-block">
                                <strong>{{ $errors->first('program_slug') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('program_desc') ? ' has-error' : '' }}">
                    <label for="program_desc" class="col-md-4 control-label">Description</label>

                    <div class="col-md-6">
                        <textarea cols="8" rows="8" id="program_desc" type="text" class="form-control" name="program_desc"> {{ @$programs->program_desc }} </textarea>

                        @if ($errors->has('program_desc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('program_desc') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-success">
                            @if (@$programs->exists)
                                Update {{ $label }}
                            @else
                                Add {{ $label }}
                            @endif
                        </button>
                        <a class="btn btn-primary" href="{{ route('program.index') }}">Back</a>

                    </div>
                </div>
              </form>

          </div>
        </div>
      </div>
    </div>
  </section>

</div>


@endsection
