@extends('layouts.admin.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Link Group Whatsapp
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List Link Grup</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    		<form class="form-horizontal">
                <div class="form-group">
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputlink" placeholder="Masukkan Link Group Whatsapp">
                  </div>
                  <button type="submit" class="btn btn-primary">Tambah Link</button>
                </div>
            </form>
          		<div class="box">
            		<div class="box-header">
              			<h3 class="box-title"></h3>
              			<div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>Nama Group</th>
                  <th>Link Group</th>
                  <th>Opsi</th>
                </tr>
                <tr>
                  <th>1</th>
                  <th>Group Satu</th>
                  <th>bit.ly/group1</th>
                  <th>
                  	<button type="button" class="btn btn-warning">Edit</button>
                  	<button type="button" class="btn btn-danger">Hapus</button>
                  </th>
                </tr>
              </table>
            </div>
              		</div>
              	</div>
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
