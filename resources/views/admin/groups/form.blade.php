@extends('layouts.admin.master')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @if (@$groups->exists)
          Perbarui Group Whatsapp
      @else
          Tambah Group Whatsapp
      @endif

    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data {{ $label }}</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
            </h3>
            <div class="box-tools">

            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            @if(@$groups->exists) <?php  $url = route('group.update', @$groups->id); ?>
            @else <?php  $url = route('group.store'); ?>
            @endif
            <form class="form-horizontal" action="{{ $url }}" method="POST">

                  @if (@$groups->exists)
                      {{ method_field('PUT') }}
                  @else
                      {{ method_field('POST') }}
                  @endif

                  {{ csrf_field() }}

                  <div class="form-group{{ $errors->has('group_name') ? ' has-error' : '' }}">
                      <label for="group_name" class="col-md-4 control-label">Nama Group</label>

                      <div class="col-md-6">
                          <input id="group_name" type="text" class="form-control" name="group_name" value="{{ @$groups->group_name }}" required>

                          @if ($errors->has('group_name'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('group_name') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                  <div class="form-group{{ $errors->has('group_link') ? ' has-error' : '' }}">
                      <label for="group_link" class="col-md-4 control-label">Link Group</label>

                      <div class="col-md-6">
                          <input id="group_link" type="text" class="form-control" name="group_link" value="{{ @$groups->group_link }}"required>

                          @if ($errors->has('group_link'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('group_link') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                  <div class="form-group{{ $errors->has('group_type') ? ' has-error' : '' }}">
                      <label for="group_type" class="col-md-4 control-label">Tipe Group</label>

                      <div class="col-md-6">
                        <select class="form-control" name="group_type" required>
                          <?php if (@$groups->group_type == "L"): ?>
                            <option value="L" selected>Laki-Laki</option>
                            <option value="P">Perempuan</option>
                          <?php else: ?>
                            <option value="L">Laki-Laki</option>
                            <option value="P" selected>Perempuan</option>
                          <?php endif; ?>
                        </select>

                          @if ($errors->has('group_type'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('group_type') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                  <div class="form-group{{ $errors->has('group_program') ? ' has-error' : '' }}">
                      <label for="group_program" class="col-md-4 control-label">Program Group</label>

                      <div class="col-md-6">
                          <select class="form-control" name="group_program" required>
                            @foreach(@$programs as $program)
                              <option value="{{$program->id}}" <?php if ($program->id == @$groups->group_program): ?>
                                selected
                              <?php endif; ?>>{{$program->program_slug}}</option>
                            @endforeach
                          </select>

                          @if ($errors->has('group_program'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('group_program') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                  <div class="form-group{{ $errors->has('group_capacity') ? ' has-error' : '' }}">
                      <label for="group_capacity" class="col-md-4 control-label">Kapasitas Group</label>

                      <div class="col-md-6">
                          <input id="group_capacity" capacity="text" class="form-control" name="group_capacity" value="{{ @$groups->group_capacity }}"required>

                          @if ($errors->has('group_capacity'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('group_capacity') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                  <div class="form-group{{ $errors->has('group_active') ? ' has-error' : '' }}">
                      <label for="group_active" class="col-md-4 control-label">Aktif Group</label>

                      <div class="col-md-6">
                        <select class="form-control" name="group_active" required>
                          <?php if (@$groups->group_active == 1): ?>
                            <option value="1" selected>Active</option>
                            <option value="0">Non Active</option>
                          <?php else: ?>
                            <option value="1">Active</option>
                            <option value="0" selected>Non Active</option>
                          <?php endif; ?>
                        </select>

                          @if ($errors->has('group_active'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('group_active') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>


                  <div class="form-group">
                      <div class="col-md-8 col-md-offset-4">
                          <button type="submit" class="btn btn-success">
                              @if (@$groups->exists)
                                  Update {{ $label }}
                              @else
                                  Add {{ $label }}
                              @endif
                          </button>
                          <a class="btn btn-primary" href="{{ route('group.index') }}">Back</a>
                      </div>
                  </div>
              </form>

          </div>
        </div>
      </div>
    </div>
  </section>

</div>


@endsection
