@extends('layouts.admin.master')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      List Group Whatsapp
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data {{ $label }}</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">
              <a class="btn btn-success" href="{{ route('group.create') }}">Create New Group</a>
            </h3>
            <div class="box-tools">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">

            @if(Session::has('alert-success'))
              <div class="alert alert-success" style="background-color:#dff0d8 !important; color:#00a65a !important">
                  <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
              </div>
              <!-- <script>
                  swal({!! Session::get('sweet_alert.alert') !!});
              </script> -->
            @endif

            <table class="table table-stripped table-hover">
              <thead>
                <th>#</th>
                <th>Nama Group</th>
                <th>Link Group</th>
                <th>Tipe Group</th>
                <th>Program Group</th>
                <th>Kapasitas Group</th>
                <th>Total Anggota Sekarang</th>
                <th>Group Aktif</th>
                <th>Opsi</th>
              </thead>
              <tbody>
                @php $no = 1; @endphp
                @foreach ($groups as $group)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $group->group_name }}</td>
                    <td>{{ $group->group_link }}</td>
                    <td>{{ $group->group_type }}</td>
                    <td>{{ $group->programs->program_slug }}</td>
                    <td>{{ $group->group_capacity}}</td>
                    <td>{{ $group->group_total_now }}</td>
                    <td class="text-center">
                      @if($group->group_active == 1)
                        <label class="label label-success">Active</label>
                      @else
                        <label class="label label-danger">Non active</label>
                      @endif
                    </td>
                    <td>
                      <form action="{{ route('group.destroy', $group->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <a class="btn btn-sm btn-warning" href="{{ route('group.edit', $group->id) }}">Edit</a>
                        <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                      </form>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>

@endsection
